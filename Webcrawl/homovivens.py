# -*- coding: utf-8 -*-
"""
Evaluation du site: 3,5/10
-> il y un certain format mais des variations sont présentes de façon sporadique 
"""
import scrapy, re
from scrapy.contrib.spiders import Rule, CrawlSpider
from scrapy.linkextractors import LinkExtractor
from datetime import datetime
from bs4 import BeautifulSoup                                                                                  
import json

from index.models.master import Text,Video 

class HomovivensSpider(CrawlSpider):
    """ciblage du site et instauration des règles de crawling."""

    name = 'Homo'
    allowed_domains = ['agora.qc.ca','encyclopedie.homovivens.org','agora-2.org']
    start_urls = ['http://www.homovivens.org/fr/encyclopedies']

    type_ressource = Text #type de ressource par défaut

    rules = (
        Rule(LinkExtractor(restrict_css=('div.texte p a')),), #on prend les liens seulement contenus dans le menu des encyclopédies
        #encyclopédie inaptitude
        #Rule(LinkExtractor(allow=('thematiques\/inaptitude.nsf'),restrict_css=('#mh2_c')),), #on se dirige vers l'index des textes dans la barre de navigation
        #Rule(LinkExtractor(allow=('inaptitude\.nsf\/Documents\/.*')), callback='parse_inap', follow=False), #parse tout le contenu d'une page ressource de inaptitude
        #Rule(LinkExtractor(allow=('inaptitude\.nsf\/Dossiers\/.*')), callback='parse_inap', follow=False), #parse tout le contenu d'un dossier ressource de inaptitude
        #encyclopédie homovivens
        #Rule(LinkExtractor(allow=('fr\/index_des_documents')), ), #on se dirige vers l'index des textes dans la barre de navigation
        #Rule(LinkExtractor(allow=('homovivens.org\/documents\/(.)')), callback='parse_homo', follow=False), #parse tout le contenu d'une page ressource de homovivens
        #encyclopédie mort
        #Rule(LinkExtractor(allow=('mort\/textes')), ), #on se dirige vers le menu des textes dans la barre de navigation
        #Rule(LinkExtractor(allow=('mort\/documents\/^[a-zA-Z_]*$'), canonicalize=True, unique=True), follow=True), #suit tous les liens de toutes les pages alphabétiques de l'index de mort
        #Rule(LinkExtractor(allow=('mort\/documents\/(.)')), callback='parse_mort', follow=False), #parse tout le contenu d'une page ressource de mort
        #encyclopédie agora
        Rule(LinkExtractor(allow=('agora.qc.ca/'),restrict_css=('div.hm_2_off a')),), #on se dirige vers l'index des thématiques dans la barre de navigation
        Rule(LinkExtractor(allow=('thematique\/^[a-zA-Z_]*$'), canonicalize=True, unique=True), follow=True), #suit tous les liens de toutes les pages alphabétiques de l'index de agora
        Rule(LinkExtractor(allow=('index\/ithematique\/.*')),), #parcours tous les liens de la page de liste de thématique
        Rule(LinkExtractor(allow=('agora.qc.ca\/Dossiers\/.*')), callback='parse_agora', follow=False), #parse tout le contenu d'une page ressource de agora
        ##encyclopédie francophonie
        #Rule(LinkExtractor(allow=('francophonie.nsf'),restrict_css=('td.mHn1_on a.mH_on')),), #on se dirige vers l'index des thématiques dans la barre de navigation
        #Rule(LinkExtractor(allow=('IndexDesSujets&s=\/^[a-zA-Z_]*$'), canonicalize=True, unique=True), follow=True), #suit tous les liens de toutes les pages alphabétiques de l'index de francophonie
        #Rule(LinkExtractor(allow=('Articles\?OpenForm&d=*')),), #parcours tous les liens de la page de liste d'articles'
        #Rule(LinkExtractor(allow=('francophonie.nsf\/Documents\/.*')), callback='parse_francophonie', follow=False), #parse tout le contenu d'une page ressource de francophonie
        ##sentiers
        ##débats
    )

    def parse_francophonie(self, response):
        pass

    def parse_agora(self, response):
        """extraction des données de l'encyclopédie agora vers la BDD."""

        self.logger.info('------------------------- %s' % response.url)

        ##traitement image
        #******************************
        if response.css('div#documents.text form div img'):
            image = response.css('div#documents.text form div img::attr(src)').extract()
        else:
            image = False
        #on supprime systématiquement l'image d'impression d'article, ce même qui est toujours le dernier élement
        if (image is not False) and ('http://www.homovivens.org/net/icons/print.png' in image):
            del image[-1]


        ##traitement video
        #******************************
        if response.css('div.video'):
            video = response.css('div.video iframe::attr(src)').extract()
            self.type_ressource = Video
            #enlève l'image du thumbnail de la video qui est le premier élément de la liste pour que la video et son image sont plaçées au début de l'article
            if image and False not in image: 
                del image[0] 
        else:
            video = False

        ##traitement contenu
        #******************************
        contenu = response.css('div#documents div').extract()
        contenu = self.fct_contenu(contenu)
        contenu = contenu.split("Date de création :")[0]

        ##traitement tags
        #******************************
        obj_brut = []
        tag = []
        ref_uniq = False
        liste_tag = ['div.container div.mkeywords a', 'div.container div.mtext', 'div.references a'] #chemin css pour mots-clés et références
        
        #on va aller chercher et ajouter chaques tags
        for l in liste_tag:
            #si on a pas déjà parsé le chemin CSS qui contient déjà les références (ref_uniq=True)
            if ((l is 'div.references a') and (ref_uniq is False)) or (l is not 'div.references a'):
                obj_brut = self.fct_tag_source(response, l, 'txt')
            #si on vient de parser le chemin CSS classque pour extraire les références et que c'était pas vide, alors on ne crawlera pas le prochain chemin CSS qui peut contenir les même références (pour éviter de mettre deux fois les même références)
            if (l is 'div.container div.mkeywords a') and (obj_brut is not ''):
                ref_uniq = True
            tag += obj_brut

        ##traitement source
        #******************************
        source = []
        liste_source = ['div.container div.mkeywords a'] #chemin css pour sources

        #on va aller chercher et ajouter chaques sources 
        for l in liste_source:
            obj_brut = self.fct_tag_source(response, l, 'href')
            source = source + obj_brut 
        

        yield{
                'titre':response.css('span#printable_area div h2::text').extract_first(), 
                'auteur':response.css('div#documents div.auteurs::text').extract_first(), 
                'url_illustration':json.dumps(image),
                'url_video':video,
                'source':source,
                'contenu':contenu,
                'tags': tag,
                'url':response.url,
                'langue':'française',
            }                

    def parse_inap(self, response):
        """extraction des données de l'encyclopédie inaptitude vers la BDD."""

        self.logger.info('------------------------- %s' % response.url)

        if 'Cette page n\'est pas disponible' not in response.css('html body div h2::text').extract():
            ##traitement contenu
            #******************************
            text_brut_inap = response.css('tr td.dTexte').extract()
            contenu_inap = self.fct_contenu(text_brut_inap) #mise en forme

            ##traitement date
            #******************************
            date_inap_brut = 'tr td.Date::text'
            date_inap = self.fct_text(response, date_inap_brut) #mise en forme
            date_inap = date_inap.replace(' ', '') #on enlève les espaces inutiles

            ##traitement tags
            #******************************
            #si on affiche bien le dossier en lien avec la ressource et pas le nombre de documents associés
            if 'Dossier' in response.css('a.BlocInverse::text'):
                tags_inap_brut = 'a.BlocInverse::text'
                tags_inap = self.fct_text(response, tags_inap_brut) #mise en forme
            else:
                tags_inap = False

            ##traitement presentation
            #******************************
            if response.css('tr td.dTexte'):
                pres = response.css('tr td.dTexte').extract()[0]
                #on enlève toutes les balises html dans l'élément
                pres = self.fct_soup(pres)
            else:
                pres = False

            yield{
                'titre':response.css('tr td.documentTitre::text').extract_first(), 
                'auteur':response.css('tr td.dAuteur::text').extract_first(), 
                'date_modif_rsc':datetime.strptime(date_inap, '%m/%d/%Y'),
                'tags':tags_inap,
                'description':pres,
                'contenu':contenu_inap,
                'url':response.url,
                'langue':'française',
            }
        else:
            print('page indisponible')

    def parse_homo(self, response):
        """extraction des données de l'encyclopédie homovivens vers la BDD."""

        self.logger.info('------------------------- %s' % response.url)

        ##traitement date
        #******************************
        date_homo_brut = response.css('span#printable_area div.dates::text').extract_first()
        date_homo = self.fct_date(date_homo_brut) #mise en forme
        date_homo = date_homo.replace(' ', '', 1) #on enlève l'espace inutile en début de chaine

        ##traitement intro
        #******************************
        if response.css('div#contenu div.intro'):
            intro = response.css('div#contenu div.intro::text').extract_first()
        else:
            intro = None

        ##traitement contenu
        #******************************
        text_brut_homo = response.css('div#contenu div.texte').extract()
        contenu_homo = self.fct_contenu(text_brut_homo) #mise en forme

        ##traitement extrait
        #******************************
        #si on trouve le titre de l'encadré extrait alors on prélève son contenu
        if response.css('div.extrait div.bloc_label'):
            extrait = response.css('div.extrait div').extract()
            extrait = ''.join(extrait) #conversion en string
            extrait = extrait.split('</div>')[1] 
            #on enlève toutes les balises html dans l'élément
            extrait = self.fct_soup(extrait)
        else:
            extrait = None

        ##traitement video
        #******************************
        if response.css('div#contenu div.video'):
            video = response.css('.video > iframe::attr(src)').extract_first()
            self.type_ressource = Video
        else:
            video = None

        yield{
            'titre':response.css('div#contenu div h2::text').extract_first(), 
            'auteur':response.css('div.auteurs::text').extract_first(), 
            'date_publication':datetime.strptime(date_homo, '%Y/%m/%d'),
            'description':intro,
            'contenu':contenu_homo,
            'extrait':extrait,
            'tags':response.css('div.dossier a::text').extract_first(),
            'url':response.url,
            'url_video':video,
            'langue':'française',
        }

    #il y a des ressources qui se rapportent à la fois à des écritures génériques et des poèmes dans le catalogue mort, cependant il n'y a aucun moyen de discener l'un de l'autre dans la structure css
    def parse_mort(self, response):
        """extraction des données de l'encyclopédie (de la) 'mort' vers la BDD."""

        self.logger.info('------------------------- %s' % response.url)

        ##traitement auteur
        #******************************
        auteur = response.css('h3 font::text').extract()
        auteur = ' '.join(auteur)
        if '  ' in auteur:
            auteur = auteur.replace('  ', ' ')

        ##traitement date de publication
        #******************************
        date_mort_brut = response.xpath('/html/body/div[1]/div/div[2]/div[4]/div[3]/span/div[2]').extract()
        date_mort = self.fct_date(date_mort_brut) #mise en forme 
        #si le premier caractère de la chaîne est un slash, on l'enlève
        if date_mort.startswith('/'):
            date_mort = date_mort.replace('/', '', 1) 
        #si il y n'y a pas assez caractères dans la version filtrée, c'est que les données brutes de l'année n'étaient au bon format, par exemple, il faudra transformer un 1 en 2001 
        if len(date_mort) < 10: 
            date_mort = '200' + date_mort

        ##traitement intro
        #******************************
        if response.css('div#documents div.text div.lead_text'):
            lead_text = response.css('div#documents div.text div.lead_text::text').extract()
            lead_text = ''.join(lead_text) #conversion en string
        else:
            lead_text = None

        ##traitement contenu
        #******************************
        text_brut_mort = response.css('div#documents div.text').extract()
        #si il y a une intro on filtre le contenu sans le premier
        if lead_text is not None:
            contenu_mort = ''.join(text_brut_mort) #conversion en string
            contenu_mort = contenu_mort.split('</div>')[1] #on sépare le contenu de l'intro   
            #on enlève toutes les balises html dans l'élément
            contenu_mort = self.fct_soup(contenu_mort)
        else:
            contenu_mort = self.fct_contenu(text_brut_mort )

        yield{
            'titre':response.css('h2::text').extract_first(), 
            'auteur':auteur, 
            'date_publication':datetime.strptime(date_mort, '%Y/%m/%d'),
            'description':lead_text,
            'contenu':contenu_mort,
            'url':response.url,
            'langue':'française',
        }



    def fct_date(self, date):
        """la fonction reçoit la date brute extraite du site et cette dernière est mise en forme."""
        #si on retrouve la chaine de caractères suivante c'est qu'on est dans la date de publication.
        date = ''.join(date) #conversion en string

        if 'Date de création:' in date:
            date_split = date.split(' | ')[0] #on sépare la date de création de la date modification
                
            #on enlève toutes les balises html dans l'élément
            date_split = self.fct_soup(date_split)

            date_split = date_split.split(':')[1] #on extrait seulement la date et pas le texte qui l'entoure
            date_split = date_split.replace('-', '/') #remplacement des tirets par des slash

            return date_split
        else:
            return False

    def fct_contenu(self, contenu):
        """la fonction reçoit le contenu brut extrait du site et ce dernier est mise en forme."""
        post_tri = [] 
        #on fusionne tous les éléments (divisés en balises div, par exemple) de contenu apres avoir enlevé les balises html
        for l in contenu:
            result = self.fct_soup(l)
            post_tri.append(result)
        post_tri = ''.join(post_tri) #conversion en string

        return post_tri

    def fct_text(self, response, data):
        """la fonction reçoit du texte brut (tag et date) extrait du site et ce dernier est mise en forme."""
        text = response.css(data).extract() #extraction
        text = ''.join(text) #conversion en string
        text = text.split(' : ')[1] #on sépare le contenu du texte

        return text

    def fct_tag_source(self, response, data, typ):
            """repère les mots-clés, références ou sources depuis des données brutes, les filtre et les extrait."""
            obj_filtered = []
            if response.css(data):
                #dans le cas d'un tag
                if typ is 'txt':
                    obj_non_filtered = response.css(data).extract() 
                    #pour chaque élément qui n'est pas un lien, pas déjà dans la liste à return et qui n'est pas vide, on met en forme et on ajoute à la liste à return
                    #possibilité d'optimisation : pour éviter des itérations inutiles des tags dans la liste obj_filtered, faire une boucle while pour stopper le for dès qu'on repère une url (signifiant qu'il n'y a plus de tags à récupérer)
                    for o in obj_non_filtered: 
                        if 'http' not in o:
                            if (o not in obj_filtered) and (o is not ''):
                                o = self.fct_soup(o)#mise en forme
                                obj_filtered.append(o)  
                #dans le cas d'une source
                else:
                    obj_non_filtered = response.css(data + '::attr(href)').extract() 
                    i=0
                    #on filtre les urls et on les ajoutent à la liste à return (on filtre par exemple des références qui redirigent vers des vues)
                    for o in obj_non_filtered:
                        if 'http' in o:
                            #on enlève les commandes javascript qui parasite l'url
                            if "javascript:Popup" in o:
                                obj_non_filtered[i] = re.search("(?P<url>https?://[^\s]+)", o).group("url") 
                            obj_filtered.append(obj_non_filtered[i])
                        i += 1
            return obj_filtered

    def fct_soup(self, data):
        """Applique le filtre de BeautifullSoup enlevant toutes les balises HTML dans une suite de données."""
        #identification des balises html ou lxml dans les données
        if BeautifulSoup(data, "html.parser").find() is True:
            soup = BeautifulSoup(data, 'html')
        else:
            soup = BeautifulSoup(data, 'lxml')
        #récupération des données qui ne sont pas des balises
        data_filtered = soup.get_text()

        return data_filtered
