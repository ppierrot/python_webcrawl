# -*- coding: utf-8 -*-
import scrapy
from scrapy.contrib.spiders import Rule , CrawlSpider
from scrapy.linkextractors import LinkExtractor

##selenium
#from pyvirtualdisplay import Display
#from selenium import webdriver

#import time


##PROBLEME ACTUEL
##sur une page de ressource, peu importe laquelle, on veut parser des données sur les commentaires
##hors, pour pouvoir avoir accès à ces fameux commentaires, il faut d'abord cliquer sur l'onglet 'evaluations' de la barre de navigation, ce même onglet étant en javascript
##Scrapy ne gére pas le javascript. Il n'est pas non plus possible d'appeler directemnt le document htlm lié à la requête GET du boutton : il faut préalablement avoir appuyé sur le boutton pour générer la requête et obtenir les données     
##une solution potentiel pourrait être la suivante :
##http://www.scrapingauthority.com/scrapy-javascript
##inconvénient étant qu'il faut installer Splash (It is created to render JS content only)
##... et pour installer Splash il faut d'abord avoir d'installé ... Docker (apparement, ça poserait des problèmes si ça venait à passer en prod')
##d'autres méthodes ont été tentées en vain comme selenium ou sur-définir crawlspider
 

##sur-définition classe crawlspider 
##se référer à https://scrapy.readthedocs.io/en/latest/topics/spiders.html#scrapy.spider.BaseSpider.start_requests 
##fonction gérant le javascript pour le bouton ->function onglet_video (view-source:http://www.canal-educatif.fr/jsvideo_new.js) 
#class CrawlSpider(scrapy.spiders.CrawlSpider):

 #   def parse_start_url(self, response):
  ##     if 'div#middle div#bas div#gauche div#ongletsvert ul li a.commentaires.ongletvideo' in response.css:
    #        #alors on change le start_urls pour "/video_"+ div +".php?id=" + videonumber + "&mode=" + mode, (exemple : http://www.canal-educatif.fr/video_presentation_new.php?id=24&mode=video)
     #       start_urls = ['http://www.canal-educatif.fr//video_presentation_new\.php\?id=\d*']


class CanalEducatifAlaDemande(CrawlSpider):
    """ciblage du site et instauration des règles de crawling."""

    name = 'CAD'
    allowed_domains = ['canal-educatif.fr']
    start_urls = ['http://www.canal-educatif.fr/']
    #start_urls = ['http://www.canal-educatif.fr/video_presentation_new.php?id=24&mode=video']

    #définition des catégories qui indexent les articles du site
    #a noter : l'ordre dans laquelle les éléments apparaissent dans la liste influence l'extraction des données tel que le titre ou le slogan/auteur
    categorie = ['art','economie','sciences']

    rules = (
        Rule(LinkExtractor(allow=('/%s\*' % categorie), deny=('\/complement\/')), follow=True), #on scrawle tous les liens de la page de liste de ressources de chacun des categories, des compléments sous forme de pdf peuvent se glisser dans des articles
        Rule(LinkExtractor(allow=('/videos/%s\d*' % categorie)), callback='parse_text', follow=False), #on va sur les liens ayant /video/ dans l'url brut et on parse tout ce qu'il nous intéresse sur la page sans suivre d'autres liens sur cette même page 
        #Rule(LinkExtractor(allow=('/videos/%s\d*' % categorie)), callback='parse_commentaire', follow=False), #
    )

    ##methode pour selenium, headless navigateur, simulateur de navigateur, à éviter car du coup il mobilise beaucoup de ressources
    #def parse_comm(self, response):

        ##selenium ne fonctionne car xvfb (emulates a dumb framebuffer using virtual memory) n'est pas installé, brew install xvfb n'est pas reconnu, il faudrait préalablement installer https://www.xquartz.org/ apparement ? ...
        
        #display = Display(visible=0, size=(1024, 768))
        #display.start()
        #browser = webdriver.Firefox()
        #browser.get("http://www.canal-educatif.fr/videos/sciences/24/pythagorecartoon/pythagore-cartoon.html")

        #elem = browser.find_element_by_xpath('/html/body/div[1]/div[2]/div[3]/div[1]/div[1]/ul/li[3]/a')
        #elem.click()
        #time.sleep(0.2)

        #elem = browser.find_element_by_xpath("//*")
        #print elem.get_attribute("outerHTML")


    def parse_commentaire(self, response):

        #filtrage de la profession de l'auteur
        prof = response.css('div#dernierscomm p.auteurcomm::text').extract_first()
        prof = prof.split(', ')[1] #on sépare la chaine de caractères en prenant comme séparateur ',', on prend la deuxième partie soit celle après le ','
            
        commentaire = {
                #'positif':,
                #'négatif':,
                #'video_attendue':,
                #'note':,
                #'evaluation':,
                #'date':,
                'profession_auteur':prof
        }

        yield {
                'commentaire': commentaire
                #'résumé':
        }


    def parse_text(self, response):
        """extraction des données dans la BDD."""

        self.logger.info('------------------------- %s' % response.url)

        #filtrage de la date de l'auteur
        auteur_oeuvre = response.css('div#infovideo p.titre::text').extract_first()
        auteur_oeuvre = auteur_oeuvre.split(' (')[0] #on sépare la chaine de caractères en prenant comme séparateur '(', on prend la deuxième partie soit celle après le '('

        #les articles catégorisés dans 'art' ont spécifiquement un auteur de l'oeuve qui y est présentée, chacun des catégories ont leur propre chemin css pour le titre
        if '/%s/' % self.categorie[0] in response.url:
            yield {
                'titre':response.css('div#infovideo p.sstitreorange::text').extract_first(),
                'auteur_oeuvre':auteur_oeuvre,
            }
        elif '/%s/' % self.categorie[1] in response.url:
            yield {
                'titre':response.css('div#infovideo p.sstitrebleu::text').extract_first(),
            }
        elif '/%s/' % self.categorie[2] in response.url:
            yield {
                'titre':response.css('div#infovideo p.sstitrevert::text').extract_first(),
            }
            
        #les articles catégorisés dans 'sciences' et 'économie' ont spécifiquement un slogan
        if ('/%s/' % self.categorie[1]) in response.url or ('/%s/' % self.categorie[2]) in response.url:
            yield {
                'slogan':response.css('div#infovideo p.sstitre2::text').extract_first(),
            }

        yield {
            'url':response.url,
            #'description':, #requête AJAX à faire ? http://www.scrapingauthority.com/scrapy-ajax
            #'url_video':,
            #'langue':'française'
        }