# -*- coding: utf-8 -*-
"""
Evaluation du site: 9/10
-> très bien structuré
"""
import scrapy
from scrapy.contrib.spiders import Rule, CrawlSpider
from scrapy.linkextractors import LinkExtractor
from datetime import datetime
from bs4 import BeautifulSoup   
import locale                                                                                            

from index.models.master import Video

class ErnestSpider(CrawlSpider):
        """ciblage du site et instauration des règles de crawling."""

        name = 'Ernest'
        allowed_domains = ['les-ernest.fr']
        start_urls = ['http://www.les-ernest.fr/']

        type_ressource = Video #type de ressource par défaut

        rules = (
            Rule(LinkExtractor(tags=('div'),attrs=('data-url')), callback='parse_ernest',follow=True),
        )

        locale.setlocale(locale.LC_TIME, "fr_FR") #converti le format de date en français

        def parse_ernest(self, response):
            """extraction des données de l'encyclopédie agora vers la BDD."""
            self.logger.info('------------------------- %s' % response.url)

            #date de publication
            date = response.css('div.post-launch.color-blue').extract_first()
            date = date.split('-->')[1] #tri
            date = date.replace('</div>','') #tri 
            date = date.strip() #enlève les espaces
            date = date.replace(' ','/') #placement du séparateur 
            #traitement spécial pour les dates qui n'auraient pas un mois abrégé et donc toujours entier
            try:
                date = datetime.strptime(date,'%b/%Y')
            except:
                date = datetime.strptime(date,'%B/%Y')
      
            #description
            desc = response.css('div.post-content').extract()
            desc = self.fct_soup(desc)

            #tags
            tags = response.css('h3 a.category-link::text').extract()

            yield{
                'titre':response.css('h2.color-green::text').extract_first(),  
                'auteur':response.css('div.post-author::text').extract_first(),   
                'url_video':response.css('iframe::attr(src)').extract(),
                'date_publication':date,
                'description':desc,
                'tags':tags,
                'url':response.url,
                'langue':'française',
            }      

        def fct_soup(self, data):
            """Applique le filtre de BeautifullSoup enlevant toutes les balises HTML dans une suite de données."""
            data = ''.join(data) #conversion en string
            #identification des balises html ou lxml dans les données
            if BeautifulSoup(data, "html.parser").find() is True:
                soup = BeautifulSoup(data, 'html')
            else:
                soup = BeautifulSoup(data, 'lxml')
            #récupération des données qui ne sont pas des balises
            data_filtered = soup.get_text()

            return data_filtered
