# -*- coding: utf-8 -*-
import scrapy
from scrapy.contrib.spiders import CrawlSpider, Rule
from scrapy.linkextractors import LinkExtractor
from scrapy.selector import Selector 
from datetime import datetime
from bs4 import BeautifulSoup

class EbooksgratuitsSpider(CrawlSpider):
    """ciblage du site et instauration des règles de crawling."""

    name = 'Ebooksgratuits'
    allowed_domains = ['ebooksgratuits.com']
    start_urls = ['https://www.ebooksgratuits.com/ebooks.php?option=search&id_auteur=&id_categorie=1&id_genre=1&begin=0&offset=100']

    rules = (
        Rule(LinkExtractor(allow=('ebooks\.php.*'), canonicalize=True,unique=True),follow=True), #explore tous les liens de la page 
        Rule(LinkExtractor(allow=('details\.php\?book=\d*')), callback='parse_detail', follow=False), # mène vers les pages de détails des e-books et s'arrête de parser une fois dedans, envoi toutes les données à la fonction 'parse_detail'
    )

    def parse_detail(self, response):
        """extraction des données dans la BDD."""

        self.logger.info('------------------------- %s' % response.url)

        """récupération et filtrage du contenu pour la description."""
        description = response.css('ul span.listeb').extract_first()
        description = description.split('<br>')[2] #on divise la cible dans un tableau par balise br et on prend le troisième élément

        """si on retrouve la chaine de caractères suivantes, ça veut dire qu'on se trouve dans l'afficahge des éditeurs et par conséquent qu'il n'y a pas de description sur cette page."""
        if('Édition' in description):
            description = ''
        else:
            """si on retrouve un chevron dans la description c'est qu'il y a des balises à retirer."""
            if "<" in description:
                """on enlève toutes les balises html dans la description."""
                soup = BeautifulSoup(description, 'html')
                description = soup.get_text()

        """récupération et filtrage du contenu pour le genre."""
        genre = response.css('ul span.listeb i::text').extract_first()
        genre = genre.replace(' ---', '') #on remplace le premier champs par le suivant

        """récupération et filtrage du contenu pour la parution."""
        parution = response.css('ul span.listeb i::text').extract()
        for p in parution:
            """si on retrouve la chaine de caractères suivantes, ça veut dire qu'on est en position de récupérer la date de parution"""
            if 'Parution' in p:
                parution = p.split(' ')[2] #c'est le troisième champs de caractères qui contient la date

        yield {
            'titre':response.css('ul span.listeb span::text').extract_first(),
            'url':response.url,
            'auteur':(response.css('ul span.auteur::text').extract_first()).lower(),
            'description':description,
            'genre':genre,
            'date_publication':parution,
        }
