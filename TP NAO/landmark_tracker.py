# -*- encoding: UTF-8 -*-

import sys
import time
import argparse
from naoqi import *


def main (ROBOT,PORT):

  #import des modules 
  #tts = ALProxy("ALTextToSpeech", ROBOT, PORT)
  mark = ALProxy("ALLandMarkDetection", ROBOT, PORT)
  moveR = ALProxy("ALMotion", ROBOT, PORT)
  posture = ALProxy("ALRobotPosture", ROBOT, PORT)
  tracker = ALProxy("ALTracker", ROBOT, PORT)

  #se lever
  posture.goToPosture("StandInit", 0.7)

  #initialiser la cible du tracker
  eventName = "LandMark"

  #taille du marqueur
  landSize = 0.095

  #id des marqueurs à identifier
  mark = [84, 80, 85, 107]

  arg = [landSize, mark]

  #définition de la cible
  tracker.registerTarget(eventName, arg)

  #mode tracking
  tracker.setMode("Move")

  #tracker.setRelativePosition([-0.5, 0.0, 0.0, 0.1, 0.1, 0.3])

  #démarrage du pistage 
  tracker.track(eventName)

  #en cours de fonctionnement
  try:
    while True:
      time.sleep(1)
  except KeyboardInterrupt:
    print 
    print "Interrompu, ferme"

  #destruction propre  
  tracker.stopTracker()  
  tracker.unregisterAllTargets()

  posture.goToPosture("Sit", 0.7)
  sys.exit(0)

if __name__ == "__main__" :

    parser = argparse.ArgumentParser()
    parser.add_argument("--ip", type=str, default="localhost",
                        help="Robot ip address.")
    parser.add_argument("--port", type=int, default=9559,
                        help="Robot port number.")
    args = parser.parse_args()

    main(args.ip, args.port)