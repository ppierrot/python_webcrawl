# -*- encoding: UTF-8 -*-

import sys
import sqlite3
import libnao_yann
from libnao_yann import *
import random

reload(sys)
sys.setdefaultencoding('utf8')

set_monitoring(True)
# Choix du robot TAO ou ZIA
init(TAO)


conn = sqlite3.connect('capitale.db')
cursor = conn.cursor()
cursor.execute("""SELECT * FROM tCapitale""")
rows = cursor.fetchall()


while (not appuyer_tete()):
    pass

# Debut du jeu
print("Jeux de la devinette")
dire("Jeux de la devinette")

# Initialisation
nbQuestions = 3 # On initialise le nombre de question que l'on va poser
ResVrai = 0  # on initialise le nombre de reponse vrai à zero

# Remplissage de la liste
#listeCapitales = [" "]
listeCapitales=''+rows[0][1]
for m in range(1,190):
    #listeCapitales.extend(["; " + rows[m][1]])
    listeCapitales += ";" + rows[m][1]
    #print(listeCapitales[m])
print(listeCapitales)
# Début du boucle
for k in range(0,nbQuestions):
    numChoixCap = random.randint(1,192) # Recupere un chiffre au hasard
    nomChoixCap = rows[numChoixCap][1] #Recupere le nom de la capitale associée au numChoixCap dans la BDD capitale.db
    nomChoixPays = rows[numChoixCap][2] # Recupere le nom du pays

    suiv = False # initialise l'etat d'avancement à False
    essaies = 0 # initialise le nombre d'essaie à 0
    maxEssaies = 2 # on fixe le nombre d'essaie(s)

    while(essaies<maxEssaies and not suiv):
        dire("Quel est la capitale de " + nomChoixPays)
        print("Quel est la capitale de " + nomChoixPays)

        print("La capitale de " + nomChoixPays + " est " + nomChoixCap)
        #reponse = raw_input()
        reponse = ecoute(listeCapitales)
        print("entendu = " +reponse)
        print("attendu = " + nomChoixCap)
        if reponse == "":
            exit()

        if(reponse == nomChoixCap):
            print("Felicitation, vous avez trouver la bonne reponse")
            dire("Félicitation, vous avez trouver la bonne reponse")
            suiv = True
            essaies = 0
            ResVrai += 1
        else:
            print("Dommage")
            dire("Dommage")
            print("Il vous reste " + str(1-essaies) + " essaies sur cette question")
            dire("Il vous reste " + str(1-essaies) + " essaies sur cette question")
            essaies += 1

print("Vous avez fini avec un score de " + str(ResVrai) + " sur " + str(nbQuestions))
dire("Vous avez fini avec un score de " + str(ResVrai) + " sur " + str(nbQuestions))

conn.close()
