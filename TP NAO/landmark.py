# -*- encoding: UTF-8 -*-

import sys
import time
import math
from libnao_yann import *
from naoqi import *

ROBOT = "localhost"
PORT = 9559
  
Projet= None
memory= None

class ProjetModule(ALModule):

  #constructeur
  def __init__(self,name):
    ALModule.__init__(self,name) 

    #import des modules 
    self.tts = ALProxy("ALTextToSpeech")
    self.mark = ALProxy("ALLandMarkDetection")
    self.moveR = ALProxy("ALMotion")
    self.posture = ALProxy("ALRobotPosture")

    #mémoire robot
    global memory
    memory = ALProxy("ALMemory")

    #event a écouter
    memory.subscribeToEvent("LandmarkDetected","Projet","Ecoute")

  #event
  def Ecoute(self,key,value,message):
    
    #désactiver l'event
    memory.unsubscribeToEvent("LandmarkDetected","Projet")

    #récupérer numéro MarkInfo
    markInfoArray = value[1]

    #identificateur marqueur
    MarkId = markInfoArray[0][1]

    #angle de détection
    rad1 = markInfoArray[0][0][1]
    rad2 = markInfoArray[0][0][2] 
    y = markInfoArray[0][0][4]
    z = 0.0

    #affichage de ce dernier
    print("MarkId : " + str(MarkId))
    print("rad1 : " + str(rad1))
    print("rad2 : " + str(rad2))
    print("y : " + str(y))

    #faire dire au robot le Mark ID
    self.tts.say("Landmark détécté " + str(markInfoArray[0][1]))

    #mise en repos
    self.posture.goToPosture("Sit",0.7)
    Projet.Onclose()
    
    #affiche : ALLandMarkDetection {TimeStamp,MarkInfo[N],CameraPoseInFrameTorso,CameraPoseInFrameRobot,CurrentCameraName
    print(str(value[1]))

    #inscription à l'évent pour le mettre en route  
    memory.subscribeToEvent("LandmarkDetected","Projet","Ecoute")   
 
  def deplacement (self,y,z,rad1,rad2):

    #se lève
    self.posture.goToPosture("StandInit", 0.7)
    time.sleep(3)

    #calcul de l'angle jusqu'au marqueur
    angle = rad1 + rad2

    #ajuster l'angle de direction
    self.moveR.moveTo(0,0,angle)

    #calcul de la distance jusqu'au marqueur
    distance = (9.5/2) / math.tan(y)
    distance /= 100

    #avance
    self.moveR.moveTo(distance-0.1,0,z)

  #fermeture
  def Onclose (self):
    memory.unsubscribeToEvent("LandmarkDetected","Projet")        

def main ():
  #listener
  broker = ALBroker ("broker", "0.0.0.0", 0, ROBOT, PORT)

  #declaration objet
  global Projet
  Projet = ProjetModule("Projet")

  #en cours de fonctionnement
  try:
    while True:
      time.sleep(1)
  except KeyboardInterrupt:
    print 
    print "Interrompu, ferme"

  #destruction propre  
  Projet.Onclose()
  broker.shutdown()
  sys.exit(0)

main()