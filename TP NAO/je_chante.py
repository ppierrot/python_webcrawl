# -*- encoding: UTF-8 -*-

import sys
import time
from naoqi import ALProxy

# Adresses IP des robots
ZIA = "10.0.8.6"
TAO = "10.0.8.4"

robot = ZIA
PORT = 9559

#on charge le module sur l'ip et le port du robot
aup = ALProxy("ALAudioPlayer", robot, PORT)

#on charge le son depuis son emplacement sur le réseau AP_NAO
fileId = aup.loadFile("/home/s4p/scripts/canfeelit.mp3")

#on démarre le son
aup.play(fileId)
