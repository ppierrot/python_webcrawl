#-*- encoding: UTF-8 -*-
import naoqi
import random
import lib_nao
from libnao_yann import *

# a c t i v e l ' a f f i c h a g e de s l o g s
set_monitoring ( True )
# i n i t i a l i s e l a l i b r a i r i e avec l e r o b o t c h o i s i
#ZIA = robot rouge, TAO = robot bleu
init (ZIA)

#consigne
#je pense à un nombre : Nao fait deviner un nombre
#entre 0 et 100, en donnant une indication : le nombre
#cherché est plus petit/grand

#méssage bienvenue
dire("bonjour pierre")
appuyer_tete()

debut = 0 #début de l'intervalle de nombres qu'il écoutera
fin = 101 #fin de l'intervalle de nombres qu'il écoutera (valeur exclut donc 10 = 11)
trouve = False #met fin à la boucle quand trouvé
val = random.randint(debut, fin) #valeur aléatoire à deviner
abandon = 0 #incrémentation les tentatives
essais = 3 #nombre d'ésais avant qu'il propose d'abandonner

#intro
dire("devinette d'un nombre entre "+str(debut)+" et "+str(fin))
dire("c'est parti !")

while not trouve:

  #on demande reponse
  dire("votre réponse ?")
  nombre = ecoute_entier(debut, fin)

  #si vide alors fin
  if nombre == "" :
    exit()

  #trouvé
  elif nombre == val :
    dire("tu as trouvé !")
    trouve = True 

  #faux
  else :
    abandon += 1
    if val > nombre :
      dire("c'est plus grand")
    else :
      dire("c'est plus petit")

  #si tous les éssais épuisés
    if abandon == essais :
      dire("veux-tu abandonner ?")
      entend = "oui; non"
      retour = ecoute(entend)
      if retour == "" :
        exit()
      elif retour == "oui" : 
        dire("la réponse était "+str(val))
        trouve = True
      else :
        abandon = 0
        dire("on continue")

#sortie    
direChanter("au revoir", pShift = 1.6)
exit()

