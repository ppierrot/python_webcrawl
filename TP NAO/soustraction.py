# -*- encoding: UTF-8 -*-

import naoqi
#import libnao_yann
#from libnao_yann import *
import lib_nao
from lib_nao import *
import random


#set_monitoring(True)
# Choix du robot TAO ou ZIA
#init(TAO)

#while (not appuyer_tete()):
    #pass

#dire("Vive les soustractions")
print("Vives les soustractions")

#dire("Choisissez les niveaux parmis un deux ou trois")
print("Choisissez les niveaux parmis 1 2 ou 3")

#lvl = ecoute_entier(1,4)
lvl = input()
if lvl == '':
    exit()

if(lvl == 1):
    y = 10
    niv = True
elif(lvl == 2):
    y = 100
    niv = True
elif(lvl == 3):
    y = 1000
    niv = True
else:
    #dire("Niveau inconnu")
    print("Niveau inconnu")
    exit()


#dire("Combien de questions voulez vous")
nbQuestions = 2

ResVrai = 0

for k in range(0,nbQuestions):
    i = random.randint(1,y)
    j = random.randint(1,y)
    if (j>i):
        b = j
        j = i
        i = b

    res = i - j
    #print(str(i) + " + " + str(j) + " = " + str(res))

    suiv = False
    essaies = 0
    maxEssaies = 2


    while(essaies<maxEssaies and not suiv):
        #dire("Que fait " + str(i) + " moins " + str(j) + " ?")
        print("Que fait " + str(i) + " - " + str(j) + " ?")
        #total = ecoute_entier(0,y+1)
        total = input()
        if total == "":
            exit()
        #total = input()
        if total == res:
            #dire("Félicitation, vous avez trouver la bonne réponse")
            print("Felicitation, vous avez trouver la bonne reponse")
            suiv = True
            essaies = 0
            ResVrai += 1
        else :
            #dire("Dommage réessaye")
            print("Dommage")
            print("il vous reste " + str(1 - essaies) + " essaies sur cette soustraction")
            essaies += 1


#dire("Vous avez un score de " + str(ResVrai) + "sur 2")
print("Vous avez un score de " + str(ResVrai) + " sur " + str(nbQuestions))
