# -*- encoding: UTF-8 -*-

import sys
import time
import almath
import argparse
from libnao_yann import *
from naoqi import *
  
ROBOT = "localhost"
PORT = 9559
  
Projet= None
memory= None

class ProjetModule(ALModule):

  #constructeur
  def __init__(self,name):
    ALModule.__init__(self,name) 

    #import des modules 
    self.tts = ALProxy("ALTextToSpeech")
    self.mark = ALProxy("ALLandMarkDetection")
    self.moveR = ALProxy("ALMotion")
    self.tracker = ALProxy("ALTracker", ROBOT, PORT)
    self.posture = ALProxy("ALRobotPosture")

    #mémoire robot
    global memory
    memory = ALProxy("ALMemory")

    #event a écouter
    memory.subscribeToEvent("LandmarkDetected","Projet","Ecoute") 


  #event
  def Ecoute(self,key,value,message):
    
    #désactiver l'event
    memory.unsubscribeToEvent("LandmarkDetected","Projet")

    #listener
    broker = ALBroker ("broker", "0.0.0.0", 0, ROBOT, PORT)

    #declaration objet
    global Projet
    Projet = ProjetModule("Projet")

    #récupérer numéro MarkInfo
    markInfoArray = value[1]

    #identificateur marqueur
    MarkId = markInfoArray[0][1]

    #angle de détection
    rad1 = markInfoArray[0][0][1]
    rad2 = markInfoArray[0][0][2] 
    y = markInfoArray[0][0][4]
    z = 0.0

    #affichage de ce dernier
    print("MarkId : " + str(MarkId))
    print("rad1 : " + str(rad1))
    print("rad2 : " + str(rad2))
    print("y : " + str(y))

    memLandmark = memory.getData("LandmarkDetected")

    angleTotal = 0
    angle = 0

    #mouvement de rotation 
    while (not memLandmark):
      tete(angle)

      if angleTotal == -50.0:
        angle = 10.0
      elif ggTotal == 50.0:
        angle = -10.0
      
      angleTotal += angle

    #landmark trouvé, arrêt tête
    motionProxy.stopMove()
    motionProxy.setStiffnesses("Head", 0.0)

    #faire dire au robot le Mark ID
    self.tts.say("Landmark détécté " + str(markInfoArray[0][1]))

    #se lever
    posture.goToPosture("StandInit", 0.7)

    #initialiser la cible du tracker
    eventName = "LandMark"

    #taille du marqueur
    landSize = 0.095

    #id des marqueurs à identifier
    mark = [84, 80, 85, 107]

    arg = [landSize, mark]

    #définition de la cible
    self.tracker.registerTarget(eventName, arg)

    #mode tracking
    self.tracker.setMode("Move")

    #tracker.setRelativePosition([-0.5, 0.0, 0.0, 0.1, 0.1, 0.3])

    #démarrage du pistage 
    self.tracker.track(eventName)

    #mise en repos
    self.posture.goToPosture("Sit",0.7)
    Projet.Onclose()
    
    #affiche : ALLandMarkDetection {TimeStamp,MarkInfo[N],CameraPoseInFrameTorso,CameraPoseInFrameRobot,CurrentCameraName
    print(str(value[1]))

    #inscription à l'évent pour le mettre en route  
    memory.subscribeToEvent("LandmarkDetected","Projet","Ecoute")   
 
  def tete(angle):
    #mise en route module  
    motionProxy.setStiffnesses("Head", 0.7)

      # Example showing multiple trajectories
      # Interpolate the head yaw to 1.0 radian and back to zero in 2.0 seconds
      # while interpolating HeadPitch up and down over a longer period.
    names  = "HeadYaw"
      # Each joint can have lists of different lengths, but the number of
      # angles and the number of times must be the same for each joint.
      # Here, the second joint ("HeadPitch") has three angles, and
      # three corresponding times.
      
    angleLists  = [angle*almath.TO_RAD]
    timeLists   = 0.7
    isAbsolute  = False
    motionProxy.angleInterpolation(names, angleLists, timeLists, isAbsolute)
    
    #destruction propre  
    Projet.Onclose()
    broker.shutdown()
    sys.exit(0)

Ecoute()
  